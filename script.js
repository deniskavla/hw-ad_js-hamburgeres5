Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40
};

Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POT_ATO = {
    price: 15,
    calories: 10
};

Hamburger.TOPPING_MAYO = {
    price: 20,
    calories: 5
};
Hamburger.TOPPING_SPICE = {
    name: "TOPPING_SPICE",
    price: 15,
    calories: 0
};

function Hamburger(size, stuffing) {
    if (size != Hamburger.SIZE_SMALL && size != Hamburger.SIZE_LARGE) {
        throw new HamburgerException ("Choose a large or small size hamburger!")
    }
    if (stuffing != Hamburger.STUFFING_CHEESE && stuffing != Hamburger.STUFFING_SALAD && stuffing != Hamburger.STUFFING_POT_ATO) {
        throw new HamburgerException ("Choose a stuffing cheese salad or pepper for a hamburger!")
    }
    this.size = size;
    this.stuffing = stuffing;//начинка
    this.topping = [];

}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.size = function () {
    return this.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.stuffing = function () {
    return this.stuffing;
};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    let newTopping = topping;
    if (this.topping.length !== null) {
        for (let i = 0; i < this.topping.length; i++) {
            if (this.topping[i].name == newTopping.name) {
                throw new HamburgerException(`Already have one ${newTopping.name}`)
            }
        }
    }
    this.topping.push(newTopping);
    return this.topping;

};


/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (!topping || !this.topping.includes(topping)) {
        throw new HamburgerException(`no such topping or topping duplicate ${topping.name}`)
    } // !topping
    return this.topping.splice(this.topping.indexOf(topping), 1);
};


/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.topping = function () {
    return this.topping;
};
/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    var number = this.size.price + this.stuffing.price;
    for (var topping of this.topping) {
        number += topping.price
    }
    return number;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    var number = this.size.calories + this.stuffing.calories;
    for (var topping of this.topping) {
        number += topping.calories
    }
    return number;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(msg) {
    this.message = msg;
    this.message = ('Check hamburger composition');
}

HamburgerException.prototype = TypeError.prototype.message;

var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

hamburger.addTopping(Hamburger.TOPPING_MAYO)

console.log(hamburger);

console.log("калорий: " + hamburger.calculateCalories());
// console.log("цена: " + hamburger.calculatePrice());
// console.log("Price: %f", hamburger.calculatePrice());
// console.log("Calories: %f", hamburger.calculateCalories());
// hamburger.addTopping(Hamburger.TOPPING_SPICE);
// console.log("Price with sauce: %f", hamburger.calculatePrice());
// // console.log("Is hamburger large: %s", hamburger.size() === Hamburger.SIZE_LARGE);
try {
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
}
catch (e) {
    console.error(e)
}
// console.log("Have %d toppings", hamburger.topping.length);


